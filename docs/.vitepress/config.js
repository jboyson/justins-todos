const isProd = process.env.NODE_ENV === "production";

module.exports = {
  title: "Justin's Todos",
  description:
    "All the things Justin is working on for GitLab broken down by milestone.",
  base: isProd ? "/justins-todos/" : "/",
  themeConfig: {
    docsDir: "docs",
    sidebar: {
      "/": [
        {
          text: "Milestones",
          children: [
            { text: "13.9", link: "/13-9" },
            { text: "13.10", link: "/13-10" },
            { text: "13.11", link: "/13-11" },
          ],
        },
      ],
    },
  },
  markdown: {
    config: (md) => {
      md.use(require("markdown-it-footnote"));
    },
  },
};
