# Rails for Frontend Developers

So you found a small tweak you want to make on gitlab.com. Fortunately for you it's open source! Now is your chance to go in and contribute! You're pretty solid with Vue and you know this part is in Vue so let's get to it! All you need to do is make this one piece of data coming from this JSON request availble. It should be pretty simple let's just go find the API call and....

Where is it? You can't find any reference to `this_data_you_want.json` anywhere. Maybe in `api.js`? Nope, nothing. You dig around a bit and find that the url is being passed in on a data attribute being passed to the Vue app. And it's coming from _gasp_...

Rails!

## Don't panic

The bad news first, Rails is huge. There is a lot to learn and most of it doesn't make a lot of sense at first if you are coming from something like Node or Express. Variables magically appear out of nowhere and there are so many ways to customize it that no two Rails projects are really the same, or very discoverable. As they say "If you've seen one Rails project, you've seen one Rails project".

The good news is that you don't need to learn all of it! Most of what Rails is capable of will not pertain to your life as a frontend developer. Most of your interactions with Rails can be boiled down to a few key concepts:

- Where is this data coming from?
- How do I tweak data that I'm getting?
- I just need to make a text change, where is the file that has it?

## Rails from 10,000 feet

The main thing that Rails tries to do is save you from mundane boilerplate. It does this by leaning heavily on convention over configuration. For example, routes are semi-dynamically generated for controllers, and the views for the those routes "just work" if you stick to the naming convention. For example the route `GET /photos/:id` will reference a `show.html.haml` template file even though the file, and the URL are never explicitly declared. 

Once you understand this, it's just a matter of learning a few conventions.

### Where variables come from

Note: I'm using the term "variables" loosely here to denote "non-static" items in a template. They could be actual variables, helpers or methods.

This one is tricky. Rails makes "variables" available from multiple locations, and there isn't really a way to tell just by looking at them. Generally speaking `@variables` are instance variables from `Controllers`. But the others can come from any of the following:

- Models
- Database columns
- Routes
- Custom helpers
- Defined in the current template
- Provided by parent templates

This can seem overwhelming, but if we narrow our scope to only frontend concerns we can focus mostly on Controller instance variables, Models and templates. This will greatly help us focus our search later.

## Mental models for translating Rails to Frontend

### Templates -> Components

It's not a direct match but you can think of individual template partials as Vue components. Let's look at an example `haml` file from GitLab

```ruby
- tab_name = local_assigns.fetch(:name, nil)
- tab_class = local_assigns.fetch(:class, nil)
- qa_selector = local_assigns.fetch(:qa_selector, nil)
- id = local_assigns.fetch(:id, nil)
- attrs = { class: [tab_class, ("active" if params[:tab] == tab_name)], data: { qa_selector: qa_selector } }
- attrs[:id] = id if id.present?

%li{ attrs }
  = yield
```

Here we're basically assigning some variables to be used in the template from the parent. Think of `local_assigns` like props in Vue speak. This is a `haml` file which is similar to `pug` and that `yield` keyword is basically a slot. If we were to rewrite this in Vue it might look something like this:

```vue
<template>
	<li :id="id" :class=[class, { active }] :data-qa_selector="qaSelector">
    <slot></slot>
  </li>
</template>

<script>
	export default {
    props: ['name', 'class', 'qaSelector', 'id'],
    computed: {
      active() {
        return this.$route.params.tab === this.name
      }
    }
  }
</script>
```

### = render -> import child component

This code `= render "projects/merge_requests/tabs/pane"` is similar to importing a child component. Note that in Rails the convention for partials is to prefix them with an underscore. So the above path would be found at `projects/merge_requests/tabs/_pane.html.haml`

### Instance variable -> Provided/Inject

Instance variables in Ruby/Rails look like this `@variable_name` and they are defined in Controllers. Templates have access to instance variables that have been defined in their scope. A good analogy of this is the "Provide/Inject" mechanism from Vue. Where the Controller in Rails is a parent component in Vue that is using `provide`. The template then is a child component using `inject`.

### params[:key] -> this.$route.params.key

The `params[:key]` is analogous to a route param in Vue. Accessed like so `this.$route.params.key` where `$route` is globally available from the Vue Router plugin, and `key` is the parameter passed in through the URL.

## Finding your way out of the woods

To try and illustrate how we might actually use this information, I've come up with a contrived hypothetical situation. NOTE: while this use case is realistic the data and properties mentioned below are completely fabricated just for this example.

When you find yourself lost it is important to establish your bearings. Fortunately as a frontend developer our tasks generally revolve around things that are rendered to the screen. So the first step is figuring out where in the app your task is located. Let's walk through a hypothetical example in GitLab on a merge request's changes tab.

![Merge requests page](./merge-requests.png)

We have a new request to add a "Last compared" timestamp after the "latest version" dropdown. We know that the data is already available from the backend, but has not been surfaced in the UI. We start by opening up DevTools and using the Vue extension to see if this is a Vue component or not. Success! We see that it is inside the `CompareVersions` component. This is our starting point.

![CompareVersions component](./CompareVersions.png)

Following the render tree we see that it has 2 ancestor components: `DiffsApp` and `MergeRequestDiffs`. Using this we trace down the `index.js` file that bootstraps the `MergeRequestsDiffs` Vue app.

Looking at `MergeRequestDiffs` we see that the Vue app is being rendered in `#js-diffs-app` and that it is initializing with data pulled from the `dataset` attribute on that element. That is our jumping off point from Rails. Now all we have to do is search for the element id "js-diffs-app". This leads us to the following line of code in one of our many `show.html.haml` files:

```ruby
= render "projects/merge_requests/tabs/pane", name: "diffs", id: "js-diffs-app", class: "diffs", data: diffs_tab_pane_data(@project, @merge_request, params)
```

This is rendering a partial template named "diffs" with an id of "js-diffs-app" a class attribute of "diffs" and a data attribute with the result from `diffs_tab_pane_data` which looks like our next stop.

Searching for `diffs_tab_pane_data` takes us to a helper file with the following definition (truncated for brevity):

```ruby
def diffs_tab_pane_data(project, merge_request, params)
  {
    "is-locked": merge_request.discussion_locked?,
    endpoint: diffs_project_merge_request_path(project, merge_request, 'json', params),
    endpoint_metadata: @endpoint_metadata_url,
    project_path: project_path(merge_request.project),
	}
end
```

It looks like our data will be in the `merge_request` argument that comes from `@merge_request`. Searching for this we are dismayed to find 616 results in 136 files 😱! Fortunately for us we now have a basic understanding of how Rails renders templates! Using this we can narrow the scope of our search.

Seeing that our template is inside the `projects/merge_requests` directory we can narrow our search there, and restrict it to only look for assigments by searching for `@merge_request =` which returns the much more reasonable 16 results in 15 files. 😅 Looking through some of these files we find a `merge_requests_controller` and `models/merge_request` which we can confidently assume have the information we're looking for.

One of the tricky bits of Rails is that it automatically exposes database columns for models. For example if you have a "Person" model that was created with a "name" property, "name" will not be listed in "models/person.rb". But if you look at the "persons" table there will be a column to hold this information. To check for available properties on a model, the simplest solution is to use `rails console` in the terminal. Now that we know we have a `models/merge_request.rb` file we can check for properties by simply typing `MergeRequest` in the rails console. 

Which returns something like this `=> MergeRequest(id: integer, target_branch: string, source_branch: string...)` listing all of the availble properties on the model. With this we confirm that we do indeed have a `compared_at` property on our `MergeRequest` model.

Now to expose the data we need we update `diffs_tab_pane_data` to add `compared_timestamp: merge_request.compared_at` and add it to the Vue app but updating `index.js`'s data object' with `comparedTimestamp: dataset.comparedTimestamp` (Note the automatic camel case conversion).

Now if we pull up DevTools again we should see our new data property:

![data object](./data-object.png)

Success! From here on out we're safely back in Vue land and should be able to finish our issue no problem.

## Summary

Rails' magic can be pretty frustrating to someone coming from a background where everything has to be configured. I know the first few times I searched for something that could not be found I got pretty flustered. But once you understand a few basics it's really not too hard to track down what you need.

- Start at the template
- Find the root element if it's a Vue app
- Look for how the data is being passed in
- Look for the associate Controller and Model
- Confirm what you're looking for in rails console

If the above still doesn't work out for you, reach out to one of our amazing backend engineers. They are all lovely people and exeedingly patient with frontenders new to rails.